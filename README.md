# The Komputer Store :computer:
The :hash: :one: place to work and buy computers at the same time!

## Getting started
Simply download the code and run it from the root folder, using your preffered method (I can recommend [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) if you are using VS Code).