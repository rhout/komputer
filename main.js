import computers from "./src/resources/computers.mock.js";
import Bank from "./src/Balance.js";
import Populator from "./src/SelectPopulator.js";
import ComputerInfoBox from "./src/ComputerInfoBox.js";
import DOMElements from "./src/DOMElements.js";

const domElements = new DOMElements();
const balances = new Bank();
const computerInfoBox = new ComputerInfoBox(onBuyComputer);

pageInit();

/**
 * Initializes the app by adding eventlisteners and populates the select.
 */
function pageInit() {
    new Populator(domElements.elComputersSelect, computers).renderSelect();

    domElements.elComputersSelect.addEventListener("change", onComputerSelected);
    domElements.elWorkBtn.addEventListener("click", onWorkClicked);
    domElements.elBankBtn.addEventListener("click", onTransferPayToBank);
    domElements.elGetLoanBtn.addEventListener("click", openPopUp);
    domElements.elTakeLoanBtn.addEventListener("click", onTakeLoan);
    domElements.elCancelBtn.addEventListener("click", closePopup);
    domElements.elCloseIcon.addEventListener("click", closePopup);
    domElements.elPayLoanBtn.addEventListener("click", onPayLoan);
    domElements.elLoanAmount.addEventListener("keyup", onKeyPress);

    window.addEventListener("click", function(event) {
        if (event.target == domElements.elPopup) {
            domElements.elPopup.style.display = "none";
        }
    })
}

/**
 * Handles key presses for the entire window.
 * @param {*} event The event that fired
 */
function onKeyPress(event) {
    if (event.key === "Enter") {
        event.preventDefault();
        onTakeLoan();
    }
    if (event.key === "Escape") {
        event.preventDefault();
        closePopup();
    }
}

/**
 * Updates the balance and rerenders the page.
 * @fires render()
 */
function onWorkClicked() {
    balances.work();
    render();
}

/**
 * Transfers the earned money to the bank paying off a bit of the loan.
 */
function onTransferPayToBank() {
    balances.transferPayToBank();
    render();
}

/**
 * Takes a loan if the user lives up to the requirements.
 */
function onTakeLoan() {
    const loanAmount = parseInt(domElements.elLoanAmount.value);
    if (balances.canGetLoan(loanAmount)) {
        balances.takeLoan(loanAmount);
        closePopup();
    } else {
        const errorMessage = isNaN(loanAmount) ? "Please enter a value." : "You can maximum loan " + (balances.bankBalance - balances.loan) * 2;
        showLoanError(errorMessage);
    }
    render();
}

/**
 * Event that triggers when the pay 
 */
function onPayLoan() {
    balances.payLoan();
    render();
}

/**
 * Shows the information of the newly selected computer.
 */
function onComputerSelected() {
    computerInfoBox.renderComputerInfoBox(getSelectedComputer());
    render();
}

/**
 * Finds the chosen computer in the dropdown.
 */
function getSelectedComputer() {
    return computers[domElements.elComputersSelect.selectedIndex - 1];
}

/**
 * Buys the selected computer if there are sufficient funds on the bank account.
 */
function onBuyComputer() {
    const computerPrice = computerInfoBox.getComputerPrice();
    if (balances.canAffordComputer(computerPrice)) {
        balances.buyComputer(computerPrice);
    }
    computerInfoBox.showPurchasedPopup(getSelectedComputer());
    render();
}

/**
 * Updates the values in the different balances.
 */
function renderBalances() {
    const { loan, bankBalance, pay } = balances;
    domElements.elBalanceText.innerText = bankBalance + loan;
    domElements.elLoanText.innerText = loan;
    if (!loan) {
        domElements.elLoanContainer.style.visibility = "hidden";
    }
    domElements.elPayText.innerText = pay;
}

/**
 * Display an error message when there is a problem lending money.
 * @param {string} errorMessage The message to display. 
 */
function showLoanError(errorMessage) {
    domElements.elLoanError.style.visibility = "visible";
    domElements.elLoanError.innerText = errorMessage;
}

/**
 * Updates the relevant fields, when a change has been made and there is no loan to be paid.
 */
function renderIsNoLoan() {
    domElements.elPayLoanBtn.style.visibility = "hidden";
    if (balances.bankBalance > 0) {
        domElements.elGetLoanBtn.disabled = false;
        domElements.elGetLoanBtn.className = "blue-btn";
    }
}

/**
 * Updates the relevant fields, when a change has been made and there is an outstanding loan.
 */
function renderIsLoan() {
    domElements.elLoanContainer.style.visibility = "visible";
    domElements.elPayLoanBtn.style.visibility = balances.pay ? "visible" : "hidden";
    domElements.elGetLoanBtn.className = "disabled-btn";
}

/**
 * Open the loan popup.
 */
function openPopUp() {
    domElements.elLoanError.style.visibility = "hidden";
    domElements.elPopup.style.display = "block";
}

/**
 * Close the loan popup.
 */
function closePopup() {
    domElements.elPopup.style.display = "none";
}

/**
 * Rerenders the page.
 */
function render() {
    if (!balances.loan) {
        renderIsNoLoan();
    } else {
        renderIsLoan();
    }
    renderBalances();

    if (balances.bankBalance < computerInfoBox.getComputerPrice()) {
        computerInfoBox.disableBuyButton();
    } else {
        computerInfoBox.enableBuyButton();
    }
}