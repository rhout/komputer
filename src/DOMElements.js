/**
 * Holds all the elements in the page, but those in the ComputerInfoBox class.
 */
class DOMElements {
    constructor() {
        // Computer dropdown container
        this.elComputersSelect = document.getElementById("select-computer");

        // Bank overview
        this.elBalanceText = document.getElementById("balance");
        this.elLoanContainer = document.getElementById("loan-container");
        this.elLoanText = document.getElementById("loan");
        this.elGetLoanBtn = document.getElementById("get-loan-btn");
        // Pay overview
        this.elWorkBtn = document.getElementById("work-btn");
        this.elBankBtn = document.getElementById("bank-btn");
        this.elPayText = document.getElementById("pay");
        this.elPayLoanBtn = document.getElementById("pay-loan-btn");

        // Modal/popup
        this.elPopup = document.getElementById("popup");
        this.elLoanAmount = document.getElementById("loan-amount");
        this.elLoanError = document.getElementById("loan-error");
        this.elTakeLoanBtn = document.getElementById("take-loan-btn");
        this.elCancelBtn = document.getElementById("cancel-btn");
        this.elCloseIcon = document.getElementById("close-icon");
    }
}

export default DOMElements;