export default [{
        "name": "Samsung",
        "price": 1000,
        "description": "This revolutionary 2-in-1 PC will defy your expectations of computing. The Samsung Galaxy Book Flex features a vivid QLED display packed with lifelike detail and stunning colors, combined with an ultra-slim, convertible design. A formidable 10th gen Intel® Core™ processor delivers split-second responsiveness, and the Wireless PowerShare turns your trackpad into a charger¹. With up to 19 hours of battery life, you can keep working and creating throughout the day and into the night.",
        "image": "https://images-na.ssl-images-amazon.com/images/I/61iSjeR5OpL._AC_SL1000_.jpg",
        "features": {
            "processor": "Intel® Core™ i7-1065G7 Processor",
            "os": "Windows 10 Home",
            "display": "15.6\" FHD QLED",
            "memory": "12 GB",
            "hardDrive": "512 SSD",
            "graphics": "Intel® Iris Plus Graphics"
        }

    },
    {
        "name": "Dell",
        "price": 1800,
        "description": "A better view: Enjoy the flexibility of wide-viewing angle (WVA) technology that allows everyone to gather around the screen to see the latest viral video. Dazzling display: An HD display (Dune) or FHD display (Titan Grey) is encased in narrow borders resulting in an expansive screen-to-body ratio and a delightful viewing experience.",
        "image": "https://i.chzbgr.com/full/8443850496/h428DC180/dell-has-fallen-upon-hard-times",
        "features": {
            "processor": "11th Generation Intel® Core™ i5-1135G7 Processor (8MB Cache, up to 4.2 GHz)",
            "os": "Windows 10 Home",
            "display": "14.0-inch FHD (1920 x 1080) WVA LED-Backlit Touch Display",
            "memory": "8GB, 1x8GB, DDR4, 3200MHz",
            "hardDrive": "256GB M.2 PCIe NVMe SSD",
            "graphics": "Intel® Iris® Xe Graphics with shared graphics memory"
        }
    },
    {
        "name": "Lenovo",
        "price": 1500,
        "description": "Enjoy heavy-duty performance on the ThinkBook 15 Gen 2 laptop, thanks to advanced 11th Gen Intel® Core™ processors enhanced by AI technology. With up to 2TB in dual-SSD storage and up to 24GB memory, this business laptop equips you for even the most intensive work. Thanks to Intelligent Cooling, you won't have to worry about it overheating.",
        "image": "https://www.lenovo.com/medias/lenovo-ideapad-3-15-almond-intel-hero.png?context=bWFzdGVyfHJvb3R8Mjc2NTE3fGltYWdlL3BuZ3xoMWUvaDU1LzEwODU5MzUxNDc0MjA2LnBuZ3wwMWIzNGFhY2M1MTljZTNiMzAwYmE3NGFjOWFiNjU2MWIxYzI4Y2I1YmJhOTM1NDVkNmZjNjVlZWYyZGEyNmIx",
        "features": {
            "processor": "11th Generation Intel® Core™ i5-1135G7 Processor (2.40 GHz, up to 4.20 GHz with Turbo Boost, 4 Cores, 8 Threads, 8 MB Cache)",
            "os": "Windows 10 Pro 64",
            "display": "15.6\" FHD (1920 x 1080) IPS, anti-glare, 250 nits",
            "memory": "8 GB DDR4 3200MHz (Soldered)",
            "hardDrive": "256 GB PCIe SSD",
            "graphics": "Integrated Intel® Iris® Xe Graphics"
        }
    },
    {
        "name": "Macintosh",
        "price": 8000,
        "description": "Some old computer that Jobs took all the credit for.",
        "image": "https://images.macrumors.com/t/XpOi1PeYh2nuyRkkS3_L_tki0hI=/400x0/filters:quality(90)/article-new/2020/01/macintosh-1984_edit.jpg?lossy",
        "features": {
            "processor": "8 MHz 68000",
            "os": "Macintosh",
            "display": "9\" monochrome",
            "memory": "128k",
            "hardDrive": "400k HDD",
            "graphics": "512x342"
        }
    },
    {
        "name": "HP",
        "price": 2000,
        "description": "Stay connected to what matters most with long-lasting battery life and thin and portable, micro-edge bezel design. Built to keep you productive and entertained from anywhere, the HP 15\" diagonal laptop features reliable performance and an expansive display - letting you stream, surf and speed through tasks from sun up to sun down.",
        "image": "https://store.hp.com/SwedenStore/Html/Merch/Images/c06222609_500x367.jpg",
        "features": {
            "processor": "10th Gen Intel® Core™ i5 processor",
            "os": "Windows 10 Home",
            "display": "15.6\" diagonal HD display",
            "memory": "12 GB",
            "hardDrive": "256 SSD",
            "graphics": "Intel® UHD"
        }
    }
]