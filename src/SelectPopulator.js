/**
 * Initiates the select element by populating it with the provided computers.
 */
class SelectPopulator {
    constructor(selectElement, computers) {
        this.elSelect = selectElement;
        this.computers = computers;
    }

    /**
     * Populates the select element.
     */
    renderSelect() {
        for (var i = 0; i < this.computers.length; i++) {
            const computer = this.computers[i];
            const option = document.createElement("option");
            option.textContent = computer.name;
            option.value = computer.name;
            this.elSelect.appendChild(option);
        }
    }
}

export default SelectPopulator;