/**
 * Keeps track of all the balances.
 */
class Balance {
    constructor() {
        this.pay = 0;
        this.loan = 0;
        this.bankBalance = 0;
    }

    /**
     * Adds money to the pay balance.
     */
    work() {
        this.pay += 100;
    }

    /**
     * Transfers the earned money to the bank spending 10% of it on paying off the loan if there is one.
     */
    transferPayToBank() {
        if (this.loan > 0) {
            const loanDeduction = this.pay * 0.1;
            const amountToDeduct = loanDeduction > this.loan ? this.loan : loanDeduction;
            this.pay -= amountToDeduct;
            this.loan -= amountToDeduct;
        }
        this.bankBalance += this.pay;
        this.pay = 0;
    }

    /**
     * Increase the loan.
     * @param {Number} loanAmount How much the loan will be.
     */
    takeLoan(loanAmount) {
        this.loan += loanAmount;
    }

    /**
     * Pays the loan with the money from the pay balance.
     */
    payLoan = () => {
        const amountToDeduct = this.pay > this.loan ? this.loan : this.pay;
        this.loan -= amountToDeduct;
        this.pay -= amountToDeduct;
    }

    /**
     * Checks if the desired loan is possible.
     * @param {Number} amount The wanted amount.
     */
    canGetLoan(amount) {
        return this.loan === 0 && amount <= this.bankBalance * 2;
    }

    /**
     * Purchases a computer by deducting its price from the bank balance.
     * @param {Number} computerPrice 
     */
    buyComputer(computerPrice) {
        this.bankBalance -= computerPrice;
    }

    /**
     * Checks if the user can afford a computer.
     * @param {Number} computerPrice 
     */
    canAffordComputer(computerPrice) {
        return computerPrice <= this.bankBalance + this.loan;
    }
}

export default Balance;