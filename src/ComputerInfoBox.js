/**
 * Holds all the html for the box containing all the computer information.
 */
class ComputerInfoBox {
    constructor(buyComputerEvent) {
        this.selectedComputer;
        this.elComputerBox = document.getElementById("buy");
        this.elComputerTitle = document.getElementById("computer-title")
        this.elComputerPrice = document.getElementById("computer-price")
        this.elComputerDescription = document.getElementById("computer-description")
        this.elComputerImage = document.getElementById("computer-image");
        this.elBuyBtn = document.getElementById("buy-btn");
        this.elBoughtPopup = document.getElementById("buy-popup");
        this.elFeatureProcessor = document.getElementById("processor");
        this.elFeatureOs = document.getElementById("os");
        this.elFeatureDisplay = document.getElementById("display");
        this.elFeatureGraphics = document.getElementById("graphics");
        this.elFeatureMemory = document.getElementById("memory");
        this.elFeatureHardDrive = document.getElementById("hard-drive");

        this.elBuyBtn.addEventListener("click", buyComputerEvent);
    }

    /**
     * Renders the box containing all the information about the selected computer.
     * @param {*} selectedComputer 
     */
    renderComputerInfoBox(selectedComputer) {
        this.elComputerTitle.innerText = selectedComputer.name;
        this.elComputerDescription.innerText = selectedComputer.description;
        this.elComputerImage.src = selectedComputer.image;
        this.elComputerImage.alt = "The computer image was supposed to be here..";
        this.elComputerPrice.innerText = selectedComputer.price;

        this.elFeatureProcessor.innerText = selectedComputer.features.processor;
        this.elFeatureOs.innerText = selectedComputer.features.os;
        this.elFeatureDisplay.innerText = selectedComputer.features.display;
        this.elFeatureGraphics.innerText = selectedComputer.features.graphics;
        this.elFeatureMemory.innerText = selectedComputer.features.memory;
        this.elFeatureHardDrive.innerText = selectedComputer.features.hardDrive;

        this.elComputerBox.style.visibility = "visible";
    }

    getComputerPrice() {
        return parseInt(this.elComputerPrice.innerText);
    }

    disableBuyButton() {
        this.elBuyBtn.classList.remove("green-btn");
        this.elBuyBtn.classList.add("disabled-btn");
    }

    enableBuyButton() {
        this.elBuyBtn.classList.add("green-btn");
        this.elBuyBtn.classList.remove("disabled-btn");
    }

    /**
     * Shows a message when a computer is purchased. This fades in and out by itself.
     * @param {*} selectedComputer 
     */
    showPurchasedPopup(selectedComputer) {
        this.elBoughtPopup.innerText = `You now own a ${selectedComputer.name}!`
        this.toggleShowPopupClass();
        setTimeout(this.toggleShowPopupClass, 5000);
    }

    /**
     * Used to make the popup start the fading in and for removing the class so it is ready to show again.
     */
    toggleShowPopupClass() {
        this.elBoughtPopup.classList.toggle("show");
    }
}

export default ComputerInfoBox;